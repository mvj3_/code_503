//1、调用 自带mail
 
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto://admin@hzlzh.com"]];
 
 
 
//2、调用 电话phone
 
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel://8008808888"]];
 
 
 
//3、调用 SMS
 
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"sms://800888"]];
 
 
 
//4、调用自带 浏览器 safari
 
[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.hzlzh.com"]];
 
 
 
//调用phone可以传递号码，调用SMS 只能设定号码，不能初始化SMS内容。